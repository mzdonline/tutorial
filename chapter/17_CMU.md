# CMU
![radio pinout](images/radiopinout.jpg)

![radio pinout physical](http://www.2x4logic.com/cmu-back-connectors-annotated.jpg)

จากรูป ซ้ายล่างเป็น port "power&more" มีขา 2S และ 2T ใช้ต่อ Serial Port TTL3.3V และอย่าลืมต่อ Ground กับตัวเครื่อง

## Resources
* http://nema.club/2014wsm/workshop%20manual/n6d09/_STARTHERE.html
* https://euroesi.mazda.co.jp/esicont/eu_eng/cx-3/20150107140637/html/id100000001400.html
* http://www.2x4logic.com/cmuconnectors.html
* http://forum.mazda6club.com/electrical/262305-2014-mazda6-wiring-diagrams.html
* https://www.youtube.com/watch?v=zFKr4L414dM

## USB
* http://www.mazdas247.com/forum/attachment.php?attachmentid=210571&d=1428099295

## Audio
* https://euroesi.mazda.co.jp/esicont/eu_eng/cx-3/20150107140637/html/id0920zz010000.html

## Display
* https://euroesi.mazda.co.jp/esicont/eu_eng/cx-3/20150107140637/html/id0920zz113600.html
* https://21stcenturyfiat124spider.wordpress.com/2016/12/13/replacing-door-speakers/#xm

## Keyboard

The following was found as well:

Turning on developer mode (/jci/scripts/developer_mode.sh enable.. after remounting / as RW and editing to uncomment the part that's commented because the file system is read-only) does "work" after a hard reboot, it gets you some overlays (which I mentioned earlier) and also allows for a working USB keyboard (however, not to actually type when the software/touch keyboard is up!).

On the keyboard:
The arrow keys work as expected
The N and M keys also work for up and down (along with arrows)
The Q, E, R, A, etc. are tied to the "shortcut" buttons on our nav control (Music, Home, Navi, Favorites, etc. but not in that order, i didn't write them down yet)
The backspace key is mapped to the back button
Enter key is mapped to pushing down on the knob


## แก้ไฟล์ cmu_dataretrieval.up
http://mazda3revolution.com/forums/2014-2017-mazda-3-skyactiv-audio-electronics/57714-infotainment-project-171.html

## MTD
```bash
/jci/bin # cat /proc/mtd
dev:    size   erasesize  name
mtd0: 00010000 00010000 "bootstrap"
mtd1: 00010000 00010000 "boot-select"
mtd2: 00020000 00010000 "ibc1"
mtd3: 00020000 00010000 "ibc2"
mtd4: 00010000 00010000 "nv-config"
mtd5: 00060000 00010000 "config"
mtd6: 00010000 00010000 "jci-boot-diag"
mtd7: 00700000 00010000 "fail-safe"
mtd8: 00020000 00010000 "update"
```
