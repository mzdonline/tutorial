#### IMPORTANT: All changes happen at your own risk!
##### Anyone who is unsure should leave it alone, ask someone with experience to help or ask in the forum.
##### I am not responsible for damages that may incur from the use.

# CMU bringback

### Problem
1. I can't login to the MZD FW56.00.513 by SSH. (Goto [Task 1](### Task 1))
2. I don't have permission to modify the files. (Goto [Task 2](### Task 2))
3. Problem still exist (### Got [Nuclear Reset](### Nuclear Reset))
4. Problem still exist (### Its another issue)

### Task 1
To connect CMU. Choosing some method

#### Method 1: Enable WiFi AP Toggle
- CMU IP : 192.168.53.1/24

##### 1.1 JCI TEST Dialogue
[WIFI_AP_Toggle](http://mazda3revolution.com/forums/2014-2017-mazda-3-skyactiv-audio-electronics/57714-infotainment-project-201.html#post1350146)

##### 1.2 USB Tweaks (In case cannot access JCI Dialogue)
https://gitlab.com/mzdonline/cmu-wifiap

#### Method 2: Terminal Console

This method may not solve the problem because of USB issue.

[![jci test](http://img.youtube.com/vi/M-iJLuxwfzU/0.jpg)](https://www.youtube.com/watch?v=M-iJLuxwfzU)



#### Method 3: USB-to-Ethernet
This method may not solve the problem because of USB issue.

- USB to Ethernet Device chipset : ASIX AX88772B
- CMU IP : 192.168.42.1/24

* http://mazda3revolution.com/forums/2014-2016-mazda-3-skyactiv-audio-electronics/57714-infotainment-project-46.html
* http://mazda3revolution.com/forums/1309434-post1636.html
* http://mazda3revolution.com/forums/1610993-post4935.html
* http://mazda3revolution.com/forums/1286058-post1557.html

#### Method 4: Serial Console
- USB to Serial device chipset : FT232RL (TTL 3.3V)

http://mazda3revolution.com/forums/2085714-post6.html

#### Method 5: Reset SPI NOR flash
- http://www.2x4logic.com/invokefailsafe.html
- [Reset CMU NOR Flash with RPi](https://translate.google.com/translate?sl=th&tl=en&js=y&prev=_t&hl=th&ie=UTF-8&u=https%3A%2F%2Fmzdonline.wordpress.com%2F2017%2F07%2F19%2Freset-cmu-nor-flash-with-rpi%2F&edit-text=)

![Connection-1](https://i1.wp.com/mzdonline.files.wordpress.com/2017/07/pi-cmu1.jpg?ssl=1&&w=375)
![Connection-2](https://i0.wp.com/mzdonline.files.wordpress.com/2017/07/img_4848.jpg?ssl=1&&w=250)

- Typing command on RPi to reset NOR flash.
- Then insert USB that included then firmare. it should install automatic.

```bash
flashrom -r backup-cmu.bin -c "S25FL064A/P" -V -p linux_spi:dev=/dev/spidev0.0
echo -ne "\x06" > /dev/spidev0.0
echo -ne "\x02\x01\x00\x00\x00" > /dev/spidev0.0
```


![connection](images/connection.jpg)

### Task 2

Ref : http://mazda3revolution.com/forums/1280610-post1532.html

### Procedure
1. Download `autorun`(https://gitlab.com/mzdonline/cmu-autorun/blob/master/installer/autorun) put in to `/data_persist/dev/bin/` on CMU.
2. Write your own `run.sh` put to SDCARD.
3. Plug SDCARD to cars. Then reboot CMU.
4. The script should execute `run.sh` only boot state.

## Details
### On cars
Write script by using Serial console or SSH connection to your car.
* Example (`/data_persist/dev/bin/autorun`)

```bash
#!/bin/sh
# /data_persist/dev/bin/autorun
echo 1 > /sys/class/gpio/Watchdog\ Disable/value
sleep 40
if [ -e /mnt/sd_nav/run.sh ]
then
  dos2unix /mnt/sd_nav/run.sh
  sh /mnt/sd_nav/run.sh
fi
```

Run command below on shell console to make sure script can properly execution.
```bash
# typing command in car
# to allow execution
dos2unix /data_persist/dev/bin/autorun
chmod +x /data_persist/dev/bin/autorun
```


Try command below for testing

### On PC (Dry run)
```bash
#!/bin/sh
# dry run example : run.sh
/jci/tools/jci-dialog --title="Ready" --text="AUTORUN install completed" --ok-label='OK' --no-cancel
```

### Real `run.sh` code to recovery `libmc_user.so`
**Be very careful** to use below script.

```bash
#!/bin/sh
# SD Card : run.sh
PATH=$PATH:/bin:/sbin:/usr/bin:/usr/sbin
echo 1 > /sys/class/gpio/Watchdog\ Disable/value
mount -o rw,remount /

#Put your command here
#Example recovery libmc_user.so
cp -a /mnt/sd_nav/libmc_user.org /jci/lib/libmc_user.so

#display confirm dialog
/jci/tools/jci-dialog --title="Good Job!" --text="File copied" --ok-label='OK' --no-cancel

#Recommend: propose protection code to prevent Bootloop
sed -i 's/watchdog_enable=\"true\"/watchdog_enable=\"false\"/g' /jci/sm/sm.conf
sed -i 's|args=\"-u /jci/gui/index.html\"|args=\"-u /jci/gui/index.html --noWatchdogs\"|g' /jci/sm/sm.conf
mount -o ro,remount /
```

Download original file `libmc_user.so` from original firmware and rename to `libmc_user.so.org`. Then copy to SDCARD. Put SDCARD to car and reboot.

Next boot the CMU will execute command by root privillege.

### Nuclear Reset
Read [this](http://mazda3revolution.com/forums/2014-2017-mazda-3-skyactiv-audio-electronics/190650-enabling-failsafe-boot-via-script.html) links . The method to active Failsafe.

#### Operation
*** Do not use Nuclear Reset for V59 ***
1. connect to CMU by Serial Console.
2. unzip `Nuclear Reset.zip`
3. copy `enable_failsafe_boot.sh` to CMU or modifed `configure_flash.sh`
4. run `./enable_failsafe_boot.sh`
5. reboot
6. format USB and copy `xxx_reinstall.up` ~~and `xxx_failsafe.up`~~. The usb should clean and has only two .up files.
7. Plug usb to ports. CMU will automatic install firmware.
